/*
 * Name : Transport.hpp
 * Author : V1t4m1n
 * Version : 2.0
 */

#include <string>
#include <thread>
#include <iostream>
#include <bitset>
#include <vector>
#include <algorithm>

#include "CacheOperation.hpp"

#ifndef DLFCN
#include <dlfcn.h>
#include "../Utils/Logging.h"
#endif

#define INSTRUCTION_THRESHOLD 3
#define BIT_INSTRUCTION_THRESHOLD 3
#define SIGNAL_SEND_LIMIT 10000

class Transport;
typedef void callback_rcv_func(Transport* transport);

// Transport's logic states
enum STATE {STARTED, SYN_ACK_WAIT, SYNC_ACK_WAIT, BIT_ACK_WAIT, FIN_ACK_WAIT, SYNC_WAIT, SYN_WAIT, BIT_WAIT, SYN_RDY, STOPPED};

enum MESSAGE {SYN, ACK, FIN, ZERO, ONE, SYNC, SYNC_ACK, BIT_ACK, NONE};

class Transport
{
private:
    STATE state; // Current state of within the transport logic
    int bitPos = 0, mode = 0; // Client or Server mode
    callback_rcv_func *callback_rcv;
    CacheOperation cacheoperation;
    void* library;
    bool data_lock, received;
    std::thread *listeningThread;
    std::bitset<8> current_bitset;
    std::vector<std::bitset<8>> received_data;
    static void startConnexion(Transport *transport);
    static MESSAGE probeForMessage(Transport *transport);
    void initCommFunctions(int mode);
    int getMode();
    void prepare_data();
    bool sendBIT(int bit);
    bool beginSession();
    bool endSession();
    void write_SYN();
    void write_ACK();
    void write_FIN();
    void write_SYNC();
    void write_SYNC_ACK();
    void stopConnexion();
public:
    COMM_Functions *myFunctions, *theirFunctions;
    bool isLocked();
    bool isReceived();
    void setReceived(bool received);
    STATE getState();
    void setState(STATE state);
    void storeBIT(MESSAGE msg);
    CacheOperation getCacheOperation();
    Transport(int mode, callback_rcv_func *callback_rcv);
    ~Transport();
    void send(std::vector<std::bitset<8>> data);
    std::vector<std::bitset<8>> getReceivedData();
    void clearReceivedData();
};




