/*
 * Name : CacheOperation.hpp
 * Author : V1t4m1n
 * Version : 2.0
 */

#define DLFCN
#include <dlfcn.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>


#include "../Utils/Logging.h"

#define THRESHOLD 130

#define CALL_NB 10

#define LIBCRYPTO_PATH "lib/libaxtls.so.1"
#define LIBCRYPTO_SQUARE_FUNCTION "bi_square"
#define LIBCRYPTO_MULTIPLY_FUNCTION "bi_terminate"
#define LIBCRYPTO_DIVIDE_FUNCTION "bi_divide"
#define LIBCRYPTO_SUBTRACT_FUNCTION "bi_subtract"
#define LIBCRYPTO_ADD_FUNCTION "bi_add"
#define LIBCRYPTO_STR_IMPORT_FUNCTION "bi_str_import"
#define LIBCRYPTO_EXPORT_FUNCTION "bi_export"
#define LIBCRYPTO_SHA1_UPDATE_FUNCTION "SHA1_Update"
#define LIBCRYPTO_SHA256_UPDATE_FUNCTION "SHA256_Update"
#define LIBCRYPTO_SHA256_FINAL_FUNCTION "SHA256_Final"
#define LIBCRYPTO_SHA512_INIT_FUNCTION "SHA512_Init"
#define LIBCRYPTO_SHA512_UPDATE_FUNCTION "SHA512_Update"
#define LIBCRYPTO_MD5_UPDATE_FUNCTION "MD5_Update"
#define LIBCRYPTO_MD5_FINAL_FUNCTION "MD5_Final"

// Communication through cache functions
typedef struct COMM_Functions
{
    void* SYN_Addr;
    void* ACK_Addr;
    void* FIN_Addr;
    void* ZERO_Addr;
    void* ONE_Addr;
    void* SYNC_Addr;
    void* SYNC_ACK_Addr;
    void* BIT_ACK_Addr;
} COMM_Functions;

class CacheOperation{
private:
    void* library;
    void *bi_export;
    void *str_import;
    void *sha1_update;
    void *sha256_update;
    void *sha256_final;
    void *sha512_init;
    void *sha512_update;
    void *md5_update;
    void *md5_final;
    void *square;
    void *bi_multiply;
    void *bi_divide;
    void *bi_subtract;
    void *bi_add;

public:
    void setLibrary(void* library);
    int probe(void *addr);
    void call(void *addr);
    void A_write_SYN();
    void B_write_SYN();
    void A_write_ACK();
    void B_write_ACK();
    void A_write_FIN();
    void B_write_FIN();
    void write_ONE();
    void write_ZERO();
    void write_SYNC_ACK();
    void write_SYNC();
    void write_BIT_ACK();
};