#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>


/* utils functions */

static void error(const char * format, ...) {
	va_list myargs;
	va_start(myargs, format);
	printf("[\033[31;1m!\033[0m] ");
	vprintf(format, myargs);
	printf("\n");
	exit(1);
}
static void info(const char * format, ...) {
	#ifdef INFO
	va_list myargs;
	va_start(myargs, format);
	printf("[\033[34;1m-\033[0m] ");
	vprintf(format, myargs);
	printf("\n");
	#endif
	return;
}
static void debug(const char * format, ...) {
	#ifdef DEBUG
	va_list myargs;
	va_start(myargs, format);
	printf("[\033[33;1mD\033[0m] ");
	vprintf(format, myargs);
	printf("\n");
	#endif
	return;
}
static void ok(const char * format, ...) {
	va_list myargs;
	va_start(myargs, format);
	printf("[\033[32;1m+\033[0m] ");
	vprintf(format, myargs);
	printf("\n");
}