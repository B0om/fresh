CC = g++
DIR = ./

clean:
	rm -f $(DIR)Build/*

info :
	mkdir -p $(DIR)Build
	$(CC) -D INFO -g -IUtils $(DIR)Fresh/Fresh.cpp $(DIR)Transport/CacheOperation.cpp $(DIR)Transport/Transport.cpp -o $(DIR)Build/Fresh -ldl -lpthread

debug :
	mkdir -p $(DIR)Build
	$(CC) -D DEBUG -g -IUtils $(DIR)Fresh/Fresh.cpp $(DIR)Transport/CacheOperation.cpp $(DIR)Transport/Transport.cpp -o $(DIR)Build/Fresh -ldl -lpthread

quiet :
	mkdir -p $(DIR)Build
	$(CC) -g -IUtils $(DIR)Fresh/Fresh.cpp $(DIR)Transport/CacheOperation.cpp $(DIR)Transport/Transport.cpp -o $(DIR)Build/Fresh -ldl -lpthread

release:
	mkdir -p $(DIR)Build
	$(CC) -IUtils $(DIR)Fresh/Fresh.cpp $(DIR)Transport/CacheOperation.cpp $(DIR)Transport/Transport.cpp -o $(DIR)Build/Fresh -ldl -lpthread

